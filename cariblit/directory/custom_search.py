from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone import api
import simplejson as json

class CustomSearch(BrowserView):


    def __call__(self):
        """"""
        self.request.response.setHeader('Content-Type',
                                'application/json; charset=utf-8')
	q = self.request.form["q"]
	catalog = api.portal.get_tool(name='portal_catalog')
	results = []
	all_brains = catalog(SearchableText=q)
	for brain in all_brains:
            results.append([brain['Title'],brain['Description'],brain.getURL()])
	try:
	    callback = self.request.form["callback"]
	
            return callback + "(" + json.dumps({"results":results}) + ")"
	except:
	    return json.dumps({"results":results})
