from zope.interface import Interface
from zope.schema import TextLine
from zope.i18nmessageid import MessageFactory
_ = MessageFactory('custom_search')

class ICustomSearchForm(Interface):

    search_string = TextLine(
        title=_(u'Search'),
        description=_(u'Enter Seach term'),
        required=False)

from Products.statusmessages.interfaces import IStatusMessage
from z3c.form import button
from z3c.form import form, field
from plone import api

class CustomSearchForm(form.Form):

    fields = field.Fields(ICustomSearchForm)
    ignoreContext = True


from plone.z3cform.layout import wrap_form
CustomSearchFormView = wrap_form(CustomSearchForm)
